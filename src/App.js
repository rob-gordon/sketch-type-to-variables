import React, { Component } from 'react';
import Close from 'react-icons/lib/fa/close';
import Plus from 'react-icons/lib/fa/plus';
import SquareO from 'react-icons/lib/fa/square-o';
import CheckSquareO from 'react-icons/lib/fa/check-square-o';
import './App.css';

const Leaf = props => {
    const { self: l, changeName, toggleLeaf } = props;
    const style = {
        fontFamily: l.font,
        fontSize: l.size + 'px',
        // lineHeight: l.lineHeight === 0 ? 1 : l.lineHeight / l.size,
        letterSpacing: l.spacing ? l.spacing : 0,
        color: `rgba(
          ${Math.round(255 * l.color.red)}, 
          ${Math.round(255 * l.color.green)}, 
          ${Math.round(255 * l.color.blue)}, 
          ${l.color.alpha})`
    };

    if (l.textTransform === 1) {
        style.textTransform = 'uppercase';
    }

    return (
        <div className={l.visible ? 'leaf' : 'leaf hidden'}>
            <span
                className="toggle-leaf"
                onClick={() => {
                    toggleLeaf(l.id);
                }}
            >
                {l.visible ? <Close /> : <Plus />}
            </span>
            <input
                type="text"
                value={l.name}
                style={style}
                onChange={e => {
                    // console.log(e.target.value)
                    changeName(l.id, e.target.value);
                }}
                disabled={!l.visible}
            />
        </div>
    );
};

const FontKey = props => {
    const { k, font } = props;
    return (
        <div>
            {k}
            <br />
            <input name="name" placeholder="name" value={font.font} />
            <br />
            <input name="weight" placeholder="weight" value={font.weight} />
        </div>
    );
};

const FontNameFix = props => {
    const rename = props.rename;
    const keys = Object.keys(rename);

    return (
        <div className="font-name-fix">
            {keys.length
                ? keys.map(k => <FontKey k={k} font={rename[k]} />)
                : ''}
        </div>
    );
};

const nameToVar = name => {
    // const Re = /[A-Za-z0-9 ]+/;
    // console.log(Re.exec(name));
    return name.toLowerCase().replace(/\W/g, '-');
};

const SingleOutput = props => {
    const { subs, includeLineHeight, includeColors } = props;
    let {
        name,
        size,
        lineHeight,
        spacing,
        font,
        visible,
        color,
        fontFamily,
        fontWeight,
        textTransform
    } = props.l;

    lineHeight =
        lineHeight == 0 ? false : Math.round(1000 * lineHeight / size) / 1000;

    size = subs['size'].active
        ? subs['size']['def'] == size
            ? null
            : `$${subs['size']['prefix']}-${subs['size']['suffixes'][size]}`
        : size + subs['size']['unit'];

    lineHeight = subs['lineHeight'].active
        ? subs['lineHeight']['def'] == lineHeight
            ? null
            : `$${subs['lineHeight']['prefix']}-${
                  subs['lineHeight']['suffixes'][lineHeight]
              }`
        : lineHeight;

    const colorRgba = `rgba(${Math.round(255 * color.red)}, ${Math.round(
        255 * color.green
    )}, ${Math.round(255 * color.blue)}, ${color.alpha})`;

    //props.subs
    return (
        <pre className="single-output">
            {`%${nameToVar(name)} {
${size ? `  font-size: ${size};\n` : ''}${
                includeLineHeight && lineHeight !== false
                    ? `  line-height: ${lineHeight};\n`
                    : ''
            }${
                includeColors ? `  color: ${colorRgba};\n` : ''
            }  font-family: ${fontFamily};\n  font-weight: ${fontWeight};\n${
                spacing ? `  letter-spacing: ${spacing}em;\n` : ``
            }${
                textTransform == 1 ? `  text-transform: 'uppercase';\n` : ``
            }}\n\n`}
        </pre>
    );
};

const Controller = props => {
    const {
        name,
        variableName,
        useToggle,
        changePrefix,
        changeSuffix,
        changeDefault
    } = props;
    const { active, def, prefix, suffixes, unit } = props.obj;
    return (
        <div className="controller">
            <label>
                <span
                    className="controller-toggler"
                    onClick={useToggle.bind(null, variableName)}
                >
                    {active ? <CheckSquareO /> : <SquareO />}
                </span>
                <i onClick={useToggle.bind(null, variableName)}>{name}</i>
            </label>
            <div
                className={
                    active ? 'controller-content' : 'controller-content hidden'
                }
            >
                <div className="left-column">
                    <label className="prefix-label">
                        <i>Prefix</i>
                        <input
                            type="text"
                            placeholder="prefix"
                            value={prefix}
                            onChange={e => {
                                changePrefix.call(
                                    null,
                                    variableName,
                                    e.target.value
                                );
                            }}
                        />
                    </label>
                    {Object.keys(suffixes).map((k, i) => (
                        <div key={i}>
                            <input
                                type="radio"
                                name={name + 'default'}
                                value={k}
                                checked={def === k}
                                onChange={e => {
                                    changeDefault.call(
                                        null,
                                        variableName,
                                        e.target.value
                                    );
                                }}
                            />
                            <i>{k}</i>
                            <input
                                type="text"
                                value={suffixes[k]}
                                onChange={e => {
                                    changeSuffix.call(
                                        null,
                                        variableName,
                                        k,
                                        e.target.value
                                    );
                                }}
                            />
                        </div>
                    ))}
                    <div>
                        <label>
                            <input
                                type="radio"
                                name={name + 'default'}
                                value={''}
                                checked={def === null}
                                onChange={e => {
                                    changeDefault.call(
                                        null,
                                        variableName,
                                        null
                                    );
                                }}
                            />
                            <i>No Default</i>
                        </label>
                    </div>
                </div>
                <div className="right-column">
                    <pre>
                        {Object.keys(suffixes).map(
                            k =>
                                k !== def
                                    ? `$${prefix}-${
                                          suffixes[k]
                                      }: ${k}${unit};\n`
                                    : ''
                        )}
                        {'\n'}
                    </pre>
                </div>
            </div>
        </div>
    );
};

class App extends Component {
    state = {
        leaves: [],
        fontSizes: [],
        rename: {},
        substitutions: {
            size: {
                active: false,
                def: null,
                prefix: 'font-size',
                unit: 'px',
                suffixes: {}
            },
            lineHeight: {
                active: false,
                def: null,
                prefix: 'line-height',
                unit: '',
                suffixes: {}
            }
        },
        includeColors: true,
        includeLineHeight: true
    };

    async componentDidMount() {
        const res = await fetch('data.json');
        const { styles: leaves } = await res.json();

        leaves.sort((a, b) => {
            return b.size - a.size;
        });

        leaves.forEach((l, i) => {
            l.id = i;
            l.visible = true;
        });
        const fonts = [...new Set(leaves.map(l => l.font))];

        /* Substitutions Set Up */
        const fontSizes = [...new Set(leaves.map(l => l.size))];
        fontSizes.sort((a, b) => a - b);
        const substitutions = {};
        substitutions['size'] = {
            active: false,
            def: null,
            prefix: 'font-size',
            unit: 'px',
            suffixes: {}
        };
        fontSizes.forEach((s, i) => {
            substitutions.size.suffixes[s] = i;
        });

        const lineHeights = [
            ...new Set(
                leaves.map(
                    l =>
                        l.lineHeight === 0
                            ? 1
                            : Math.round(1000 * l.lineHeight / l.size) / 1000
                )
            )
        ];
        lineHeights.sort((a, b) => a - b);
        substitutions['lineHeight'] = {
            active: false,
            def: null,
            prefix: 'line-height',
            unit: '',
            suffixes: {}
        };
        lineHeights.forEach((s, i) => {
            substitutions.lineHeight.suffixes[s] = i;
        });

        const rename = {};
        fonts.forEach(f => {
            rename[f] = { font: '', weight: '' };
        });
        this.setState({ leaves, rename, fontSizes, substitutions });
    }

    changeLeafName = (id, newName) => {
        console.log(id, newName);
        const leaves = this.state.leaves;
        const leaf = leaves.filter(l => l.id === id)[0];
        leaf.name = newName;
        this.setState({ leaves });
    };

    toggleLeaf = id => {
        const leaves = this.state.leaves;
        let index = 0;
        const leaf = leaves.filter((l, i) => {
            if (l.id === id) index = i;
            return l.id === id;
        })[0];
        leaf.visible = !leaf.visible;

        leaves.sort((a, b) => {
            if (!b.visible) return -1;
            if (!a.visible) return 1;
            if (b.size > a.size) return 1;
            if (b.size < a.size) return -1;
            if (b.fontWeight > a.fontWeight) return 1;
            if (b.fontWeight > a.fontWeight) return -1;
            if (
                typeof a.fontFamily === 'string' &&
                typeof b.fontFamily === 'string'
            ) {
                return b.fontFamily.toLowerCase() - a.fontFamily.toLowerCase();
            } else {
                return -1;
            }
        });

        this.setState({ leaves });
    };

    toggleControllerUse = name => {
        const subsitutions = this.state.substitutions;
        subsitutions[name].active = !subsitutions[name].active;
        this.setState({ subsitutions });
    };

    toggleControllerPrefixChange = (name, newPrefix) => {
        const substitutions = this.state.substitutions;
        substitutions[name].prefix = newPrefix;
        this.setState({ substitutions });
    };

    changeControllerSuffix = (name, suffix, newValue) => {
        console.log(name, suffix, newValue);
        const substitutions = this.state.substitutions;
        substitutions[name].suffixes[suffix] = newValue;
        this.setState({ substitutions });
    };

    changeControllerDefault = (name, newDefault) => {
        const substitutions = this.state.substitutions;
        substitutions[name].def = newDefault;
        this.setState({ substitutions });
    };

    toggleIncludeColors = () => {
        const inc = !this.state.includeColors;
        this.setState({ includeColors: inc });
    };

    toggleIncludeLineHeight = () => {
        const inc = !this.state.includeLineHeight;
        this.setState({ includeLineHeight: inc });
    };

    copyCode = () => {
        const allPres = document.querySelectorAll('pre');
        let scss = '';
        allPres.forEach(p => {
            if (p.offsetParent !== null) {
                scss += p.innerText;
            }
        });
        var textarea = document.createElement('textarea');
        textarea.textContent = scss;
        textarea.style.position = 'fixed'; // Prevent scrolling to bottom of page in MS Edge.
        document.body.appendChild(textarea);
        textarea.select();
        try {
            return document.execCommand('copy'); // Security exception may be thrown by some browsers.
        } catch (ex) {
            console.warn('Copy to clipboard failed.', ex);
            return false;
        } finally {
            document.body.removeChild(textarea);
        }
    };

    render() {
        const {
            leaves,
            rename,
            fontSizes,
            toggleControllerUse,
            includeColors,
            includeLineHeight
        } = this.state;
        return (
            <div className="App">
                <div className="control-bar">
                    <button onClick={this.copyCode}>Copy</button>
                </div>
                <div className="includes">
                    <h2>Includes</h2>
                    <label>
                        <input
                            type="checkbox"
                            checked={includeColors}
                            onChange={this.toggleIncludeColors}
                        />
                        <i>Colors</i>
                    </label>
                    <label>
                        <input
                            type="checkbox"
                            checked={includeLineHeight}
                            onChange={this.toggleIncludeLineHeight}
                        />
                        <i>Line Height</i>
                    </label>
                </div>
                <div className="variables">
                    <h2>Create Variables</h2>
                    <Controller
                        name={'Font Size'}
                        obj={this.state.substitutions.size}
                        variableName={'size'}
                        useToggle={this.toggleControllerUse}
                        changeSuffix={this.changeControllerSuffix}
                        changeDefault={this.changeControllerDefault}
                        changePrefix={this.toggleControllerPrefixChange}
                    />
                    <Controller
                        name={'Line Height'}
                        obj={this.state.substitutions.lineHeight}
                        variableName={'lineHeight'}
                        useToggle={this.toggleControllerUse}
                        changeSuffix={this.changeControllerSuffix}
                        changeDefault={this.changeControllerDefault}
                        changePrefix={this.toggleControllerPrefixChange}
                    />
                </div>
                <div className="leaves">
                    <div className="controls">&nbsp;</div>
                    {leaves.length
                        ? leaves.map((l, i) => (
                              <div className="leaf-row" key={`${l.id}`}>
                                  <Leaf
                                      self={l}
                                      //   changeName={this.changeLeafName}
                                      changeName={(a, b) => {
                                          //   console.log(a, b)
                                          this.changeLeafName(a, b);
                                      }}
                                      toggleLeaf={this.toggleLeaf}
                                  />
                                  <SingleOutput
                                      l={l}
                                      subs={this.state.substitutions}
                                      includeColors={this.state.includeColors}
                                      includeLineHeight={
                                          this.state.includeLineHeight
                                      }
                                  />
                              </div>
                          ))
                        : 'Loading'}
                </div>
            </div>
        );
    }
}

export default App;
