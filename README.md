# Sketch Type Styles to SCSS Vars

## TO INSTALL

### 1. Install Sketch Plugin [shared-text-styles](https://github.com/nilshoenson/shared-text-styles)

### 2. Export sketch text styles to the __./public__ folder as `data.json`

### 3. In the terminal, `cd` into this folder.

### 4. `npm install` 

----

## TO RUN

### 1. `npm run start`

----

#### TO-DO's

- The biggest todo is to get __EVERYTHING__ out of the document. Not just what has been added to the global text styles.
  - Then remove duplicates
  - Organize in a tree space, alternating dimensionality, with ghost apples
- Turn __Includes__ and __Variablize__ into an upper navigation bar.
- Add a great .gif background.
- Show/Hide SCSS
- Add Variablize for font-families (needs a font-weight movie) and Colors.
- Add real letter spacing to result
- Also automatically get rid of duplicate styles...